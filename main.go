package main

import (
	"gitlab.com/geraldsamosir/go-api-run/src/config/database"
	"gitlab.com/geraldsamosir/go-api-run/src/repository"
	"gitlab.com/geraldsamosir/go-api-run/src/service/article"
	"gitlab.com/geraldsamosir/go-api-run/src/service/movie"
	"gitlab.com/geraldsamosir/go-api-run/src/transport/grpc"
	"gitlab.com/geraldsamosir/go-api-run/src/transport/webserver"
)

var ws webserver.WebServer
var gps grpc.GrpcServer

func main() {

	var db database.DatabaseInstance
	connection := db.GetConnection()
	articleRepo := repository.NewArticle(connection)
	articleSvc := article.NewArticle(articleRepo)
	movieSvc := movie.NewMovie()

	go func() {
		gps.Run(articleSvc)
	}()

	ws.Run(articleSvc, movieSvc)

}
