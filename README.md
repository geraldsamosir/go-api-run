# Go Api Run

## Generate proto golang

```
protoc -I . --go_opt=module=gitlab.com/geraldsamosir/go-api-run --go_out=src/transport/grpc/proto/ --go-grpc_opt=module=gitlab.com/geraldsamosir/go-api-run --go-grpc_out=src/transport/grpc/proto/  src/transport/grpc/proto/*.proto

```

## for testing

```
go test ./... for all
go test ./src/service/article to spesifict package
```
