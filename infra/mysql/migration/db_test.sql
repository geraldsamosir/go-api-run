use testdb;

create table articles (
 ar_id int(11) unsigned AUTO_INCREMENT,
 ar_title varchar(100) not null,
 ar_body varchar(255) not null,
 ar_tags_id json not null,
 primary key(ar_id)
);

create table tags (
  t_id int(11) unsigned AUTO_INCREMENT,
  t_name varchar(100) not null,
  primary key(t_id)
);
