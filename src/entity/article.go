package entity

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
)

type ArticleEntity struct {
	Title   string  `json:"title" db:"ar_title" validate:"required"`
	Body    string  `json:"body"  db:"ar_body" validate:"required"`
	TagsIds TagsIds `json:"tags_id" db:"ar_tags_id" validate:"required"`
}

type TagsIds []int64

func (tgs *TagsIds) Scan(val interface{}) error {
	switch v := val.(type) {
	case []byte:
		json.Unmarshal(v, &tgs)
		return nil
	case string:
		json.Unmarshal([]byte(v), &tgs)
		return nil
	default:
		return errors.New(fmt.Sprintf("Unsupported type: %T", v))
	}
}

func (tgs *TagsIds) Value() (driver.Value, error) {
	return json.Marshal(tgs)
}

type ArticleEntityResponse struct {
	Id      int     `json:"id" db:"ar_id"`
	Title   string  `json:"title" db:"ar_title"`
	Body    string  `json:"body" db:"ar_body"`
	TagsIds TagsIds `json:"tags_id" db:"ar_tags_id""`
}
