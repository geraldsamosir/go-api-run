package database

import (
	"fmt"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
)

type DatabaseInstance struct {
	hostName           string
	user               string
	password           string
	dbName             string
	SetConnMaxIdleTime int
	SetConnMaxLifetime int
}

type DB struct {
	*sqlx.DB
}

func (di *DatabaseInstance) GetConnection() *DB {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	di.dbName = os.Getenv("DATABASE_NAME")
	di.user = os.Getenv("DATABASE_USER")
	di.hostName = os.Getenv("DATABASE_HOST")
	di.password = os.Getenv("DATABASE_PASSWORD")
	di.SetConnMaxIdleTime = 2
	di.SetConnMaxLifetime = 1

	db, err := sqlx.Open("mysql", fmt.Sprintf("%s:%s@tcp(localhost:3306)/%s?parseTime=true", di.user, di.password, di.dbName))

	if err != nil {
		panic(err)
	}

	db.SetMaxIdleConns(10)
	db.SetMaxOpenConns(100)
	db.SetConnMaxIdleTime(time.Duration(di.SetConnMaxIdleTime) * time.Minute)
	db.SetConnMaxLifetime(time.Duration(di.SetConnMaxLifetime) * time.Minute)

	return &DB{db}

}
