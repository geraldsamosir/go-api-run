package movie

import (
	"context"

	"gitlab.com/geraldsamosir/go-api-run/src/entity"
)

type Movie interface {
	FindAll(ctx context.Context, title string, page string) ([]entity.Movie, error)
}
