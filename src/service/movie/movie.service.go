package movie

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/geraldsamosir/go-api-run/src/entity"
)

const (
	BASEURL = "https://www.omdbapi.com/"
	APIKEY  = "faf7e5bb&s="
)

type MovieServices struct {
}

func NewMovie() Movie {
	return &MovieServices{}
}

func (ms MovieServices) FindAll(ctx context.Context, title string, page string) ([]entity.Movie, error) {

	var err error
	var client = &http.Client{}
	data := entity.Movies{}

	url := fmt.Sprintf("%s?apikey=%s%s&page=%s", BASEURL, APIKEY, title, page)

	request, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return []entity.Movie{}, err
	}

	res, err := client.Do(request)
	if err != nil {
		return []entity.Movie{}, err
	}
	defer res.Body.Close()

	body, _ := io.ReadAll(res.Body)

	err = json.Unmarshal(body, &data)
	if err != nil {
		return []entity.Movie{}, err
	}

	if len(data.Search) == 0 {
		return []entity.Movie{}, nil
	}

	return data.Search, nil
}
