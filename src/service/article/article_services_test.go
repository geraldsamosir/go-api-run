package article

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/geraldsamosir/go-api-run/src/entity"
	"gitlab.com/geraldsamosir/go-api-run/src/repository"
)

var articleRepo = &repository.ArticleRepositoryMock{Mock: mock.Mock{}}
var articleServices = ArticleServices{articleRepository: articleRepo}

func TestFindByID(t *testing.T) {
	var ctx context.Context

	articleRepo.Mock.On("FindById", ctx, 1).Return(nil).Once()

	article, err := articleServices.FindById(ctx, 1)

	assert.NotNil(t, article)
	assert.NotNil(t, err)

}

func TestFindByIDQueryError(t *testing.T) {
	var ctx context.Context

	articleData := entity.ArticleEntityResponse{Title: "title", Body: "body"}

	articleRepo.Mock.On("FindById", ctx, 1).Return(articleData).Once()

	article, err := articleServices.FindById(ctx, 1)

	assert.NotNil(t, article)
	assert.Equal(t, article.Title, articleData.Title)
	assert.Nil(t, err)
}
