package article

import (
	"context"

	"gitlab.com/geraldsamosir/go-api-run/src/entity"
	"gitlab.com/geraldsamosir/go-api-run/src/repository"
)

type ArticleServices struct {
	articleRepository repository.ArticleRepo
}

func NewArticle(AR repository.ArticleRepo) Article {
	return &ArticleServices{
		articleRepository: AR,
	}
}

func (as ArticleServices) FindAll(ctx context.Context) ([]entity.ArticleEntityResponse, error) {

	articles, err := as.articleRepository.FindAll(ctx)

	if err != nil {
		return articles, err
	}

	return articles, nil
}

func (as ArticleServices) FindById(ctx context.Context, Id int) (entity.ArticleEntityResponse, error) {

	article, err := as.articleRepository.FindById(ctx, Id)

	if err != nil {
		return article, err
	}

	return article, nil
}

func (as ArticleServices) Create(ctx context.Context, payload entity.ArticleEntity) (bool, error) {
	_, err := as.articleRepository.Create(ctx, payload)

	if err != nil {
		return false, err
	}

	return true, nil
}

func (as ArticleServices) Update(ctx context.Context, Id int, payload entity.ArticleEntity) (bool, error) {
	_, err := as.articleRepository.Update(ctx, Id, payload)

	if err != nil {
		return false, err
	}

	return true, nil
}
