package repository

import (
	"context"

	"gitlab.com/geraldsamosir/go-api-run/src/entity"
)

type ArticleRepo interface {
	FindAll(ctx context.Context) ([]entity.ArticleEntityResponse, error)
	FindById(ctx context.Context, Id int) (entity.ArticleEntityResponse, error)
	Create(ctx context.Context, payload entity.ArticleEntity) (bool, error)
	Update(ctx context.Context, Id int, payload entity.ArticleEntity) (bool, error)
}
