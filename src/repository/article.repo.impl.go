package repository

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/geraldsamosir/go-api-run/src/config/database"
	"gitlab.com/geraldsamosir/go-api-run/src/entity"
)

type ArticleRepoImpl struct {
	db *database.DB
}

func NewArticle(dbConn *database.DB) ArticleRepo {
	return &ArticleRepoImpl{db: dbConn}
}

func (ar *ArticleRepoImpl) FindAll(ctx context.Context) ([]entity.ArticleEntityResponse, error) {
	articles := []entity.ArticleEntityResponse{}
	err := ar.db.SelectContext(ctx, &articles, "select ar_id, ar_title, ar_body, ar_tags_id from articles")

	if err != nil {
		fmt.Println("articleRepo FindAll", err.Error())
		return articles, err
	}

	return articles, nil
}

func (ar *ArticleRepoImpl) FindById(ctx context.Context, Id int) (entity.ArticleEntityResponse, error) {
	article := entity.ArticleEntityResponse{}

	err := ar.db.GetContext(ctx, &article, `select ar_id, ar_title, ar_body, ar_tags_id from articles where ar_id=?`, Id)

	if err != nil {
		fmt.Println("articleRepo FindById", err.Error())
		return article, err
	}

	return article, nil
}

func (ar *ArticleRepoImpl) Create(ctx context.Context, payload entity.ArticleEntity) (bool, error) {

	tagIds, _ := json.Marshal(payload.TagsIds)

	_, err := ar.db.NamedExecContext(ctx, `
	      insert into articles (ar_title, ar_body, ar_tags_id) 
		  values(:ar_title, :ar_body, :ar_tags_id)`,
		map[string]any{
			"ar_title":   payload.Title,
			"ar_body":    payload.Body,
			"ar_tags_id": tagIds,
		})

	if err != nil {
		fmt.Println("articleRepo  Create", err.Error())
		return false, err
	}

	return true, nil
}

func (ar *ArticleRepoImpl) Update(ctx context.Context, Id int, payload entity.ArticleEntity) (bool, error) {

	tagIds, _ := json.Marshal(payload.TagsIds)

	_, err := ar.db.NamedExecContext(ctx, `
	   update articles set ar_title=:ar_title, ar_body=:ar_body, ar_tags_id=:ar_tags_id
	   where ar_id=:ar_id 		
	`,
		map[string]any{
			"ar_title":   payload.Title,
			"ar_body":    payload.Body,
			"ar_tags_id": tagIds,
			"ar_id":      Id,
		})

	if err != nil {
		fmt.Println("articleRepo  Update", err.Error())
		return false, err
	}

	return true, nil
}
