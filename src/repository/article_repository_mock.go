package repository

import (
	"context"
	"errors"

	"github.com/stretchr/testify/mock"
	"gitlab.com/geraldsamosir/go-api-run/src/entity"
)

type ArticleRepositoryMock struct {
	Mock mock.Mock
}

func (am ArticleRepositoryMock) FindAll(ctx context.Context) ([]entity.ArticleEntityResponse, error) {
	return []entity.ArticleEntityResponse{}, nil
}

func (am ArticleRepositoryMock) FindById(ctx context.Context, Id int) (entity.ArticleEntityResponse, error) {
	arg := am.Mock.Called(ctx, Id)

	if arg.Get(0) == nil {
		return entity.ArticleEntityResponse{}, errors.New("some error")
	} else {
		article := arg.Get(0).(entity.ArticleEntityResponse)

		return article, nil
	}
}

func (am *ArticleRepositoryMock) Create(ctx context.Context, payload entity.ArticleEntity) (bool, error) {
	return true, nil
}

func (am *ArticleRepositoryMock) Update(ctx context.Context, Id int, payload entity.ArticleEntity) (bool, error) {
	return true, nil
}
