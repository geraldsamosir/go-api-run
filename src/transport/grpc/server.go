package grpc

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/geraldsamosir/go-api-run/src/service/article"
	"gitlab.com/geraldsamosir/go-api-run/src/transport/grpc/auth"
	"google.golang.org/grpc"
)

type GrpcServer struct {
}

func (gps *GrpcServer) Run(as article.Article) {
	var port string = ":50051"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	interceptor := auth.NewAuthInterceptor()

	s := grpc.NewServer(
		grpc.UnaryInterceptor(interceptor.Unary()),
		grpc.StreamInterceptor(interceptor.Stream()),
	)

	NewArticleHandler(s, as)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

	fmt.Println("Grpc run in ", port)

}
