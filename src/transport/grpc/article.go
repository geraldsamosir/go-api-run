package grpc

import (
	"context"

	"gitlab.com/geraldsamosir/go-api-run/src/entity"
	"gitlab.com/geraldsamosir/go-api-run/src/helper"
	"gitlab.com/geraldsamosir/go-api-run/src/service/article"
	pb "gitlab.com/geraldsamosir/go-api-run/src/transport/grpc/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ArticleHanlder struct {
	articleSvc article.Article
	pb.ArticleServer
}

func NewArticleHandler(s *grpc.Server, as article.Article) {
	handler := &ArticleHanlder{articleSvc: as}

	pb.RegisterArticleServer(s, handler)
}

func (ah *ArticleHanlder) FindAll(ctx context.Context, _ *pb.Empty) (*pb.ListArticles, error) {

	articles, err := ah.articleSvc.FindAll(ctx)

	if err != nil {
		return nil, status.Errorf(codes.Unknown, "failed to get list article")
	}

	listArticles := make([]*pb.Articles, 0, 10)

	for _, article := range articles {
		tagsId64 := make([]int64, 0, 10)

		for _, tagId := range article.TagsIds {
			tagsId64 = append(tagsId64, int64(tagId))
		}

		listArticles = append(listArticles, &pb.Articles{
			Id:     int64(article.Id),
			Title:  article.Title,
			Body:   article.Body,
			TagsId: tagsId64,
		})
	}

	return &pb.ListArticles{Success: true, Articles: listArticles}, nil
}

func (ah *ArticleHanlder) FindById(ctx context.Context, in *pb.FilterById) (*pb.Articles, error) {
	Id := in.GetId()

	article, err := ah.articleSvc.FindById(ctx, int(Id))

	if err != nil {
		return nil, status.Errorf(codes.NotFound, "article not found")
	}

	tagsId64 := make([]int64, 0, 10)

	for _, tagId := range article.TagsIds {
		tagsId64 = append(tagsId64, int64(tagId))
	}

	return &pb.Articles{
		Id:     int64(article.Id),
		Title:  article.Title,
		Body:   article.Body,
		TagsId: tagsId64,
	}, nil
}

func (ah *ArticleHanlder) Create(ctx context.Context, in *pb.NewArticle) (*pb.UpSertArticleResponse, error) {

	articlePayload := entity.ArticleEntity{
		Title:   in.Title,
		Body:    in.Body,
		TagsIds: in.TagsId,
	}

	validation := helper.ValidationRequest{}

	if err := validation.ValidateHandling(&articlePayload); err != nil {
		return nil, status.Errorf(codes.Unknown, "payload uncompleted")
	}

	_, err := ah.articleSvc.Create(ctx, articlePayload)

	if err != nil {
		return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
	}

	return &pb.UpSertArticleResponse{Success: true, Message: "Berhasil membuat Artikel"}, nil
}

func (ah *ArticleHanlder) Update(ctx context.Context, in *pb.UpdateArticle) (*pb.UpSertArticleResponse, error) {
	id := int(in.Id)

	if in.Article == nil {
		return nil, status.Errorf(codes.Unknown, "payload uncompleted")
	}

	articlePayload := entity.ArticleEntity{
		Title:   in.Article.Title,
		Body:    in.Article.Body,
		TagsIds: in.Article.TagsId,
	}

	validation := helper.ValidationRequest{}

	if err := validation.ValidateHandling(&articlePayload); err != nil {
		return nil, status.Errorf(codes.Unknown, "payload uncompleted")
	}

	_, err := ah.articleSvc.Update(ctx, id, articlePayload)

	if err != nil {
		return nil, status.Errorf(codes.Unimplemented, "method update not implemented")
	}

	return &pb.UpSertArticleResponse{Success: true, Message: "Berhasil mengupdate Artikel"}, nil
}
