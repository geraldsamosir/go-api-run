package auth

import (
	"context"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func NewAuthInterceptor() *AuthInterceptors {
	return &AuthInterceptors{}
}

type AuthInterceptors struct{}

func (auth *AuthInterceptors) Unary() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp any, err error) {

		log.Println("--> unary interceptor: ", info.FullMethod)

		err = auth.authentication(ctx)

		if err != nil {
			return nil, err
		}

		return handler(ctx, req)
	}
}

func (auth *AuthInterceptors) Stream() grpc.StreamServerInterceptor {
	return func(srv any, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		log.Println("--> stream interceptor: ", info.FullMethod)

		return handler(srv, stream)
	}
}

func (auth *AuthInterceptors) authentication(ctx context.Context) error {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return status.Errorf(codes.Unauthenticated, "metadata is not provided")
	}

	//log.Println("---> all meta data sending", md)

	jwt, ok := md["auth"]

	if !ok {
		log.Println("exist", ok)
		return status.Errorf(codes.Unauthenticated, "meta data for authentication not provided")
	}

	log.Println("--> meta data auth", jwt[0])

	return nil
}
