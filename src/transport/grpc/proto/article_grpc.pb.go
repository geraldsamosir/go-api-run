// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v4.25.1
// source: src/transport/grpc/proto/article.proto

package go_api_run

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ArticleClient is the client API for Article service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ArticleClient interface {
	FindAll(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*ListArticles, error)
	FindById(ctx context.Context, in *FilterById, opts ...grpc.CallOption) (*Articles, error)
	Create(ctx context.Context, in *NewArticle, opts ...grpc.CallOption) (*UpSertArticleResponse, error)
	Update(ctx context.Context, in *UpdateArticle, opts ...grpc.CallOption) (*UpSertArticleResponse, error)
}

type articleClient struct {
	cc grpc.ClientConnInterface
}

func NewArticleClient(cc grpc.ClientConnInterface) ArticleClient {
	return &articleClient{cc}
}

func (c *articleClient) FindAll(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*ListArticles, error) {
	out := new(ListArticles)
	err := c.cc.Invoke(ctx, "/proto.Article/FindAll", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *articleClient) FindById(ctx context.Context, in *FilterById, opts ...grpc.CallOption) (*Articles, error) {
	out := new(Articles)
	err := c.cc.Invoke(ctx, "/proto.Article/FindById", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *articleClient) Create(ctx context.Context, in *NewArticle, opts ...grpc.CallOption) (*UpSertArticleResponse, error) {
	out := new(UpSertArticleResponse)
	err := c.cc.Invoke(ctx, "/proto.Article/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *articleClient) Update(ctx context.Context, in *UpdateArticle, opts ...grpc.CallOption) (*UpSertArticleResponse, error) {
	out := new(UpSertArticleResponse)
	err := c.cc.Invoke(ctx, "/proto.Article/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ArticleServer is the server API for Article service.
// All implementations must embed UnimplementedArticleServer
// for forward compatibility
type ArticleServer interface {
	FindAll(context.Context, *Empty) (*ListArticles, error)
	FindById(context.Context, *FilterById) (*Articles, error)
	Create(context.Context, *NewArticle) (*UpSertArticleResponse, error)
	Update(context.Context, *UpdateArticle) (*UpSertArticleResponse, error)
	mustEmbedUnimplementedArticleServer()
}

// UnimplementedArticleServer must be embedded to have forward compatible implementations.
type UnimplementedArticleServer struct {
}

func (UnimplementedArticleServer) FindAll(context.Context, *Empty) (*ListArticles, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FindAll not implemented")
}
func (UnimplementedArticleServer) FindById(context.Context, *FilterById) (*Articles, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FindById not implemented")
}
func (UnimplementedArticleServer) Create(context.Context, *NewArticle) (*UpSertArticleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedArticleServer) Update(context.Context, *UpdateArticle) (*UpSertArticleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedArticleServer) mustEmbedUnimplementedArticleServer() {}

// UnsafeArticleServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ArticleServer will
// result in compilation errors.
type UnsafeArticleServer interface {
	mustEmbedUnimplementedArticleServer()
}

func RegisterArticleServer(s grpc.ServiceRegistrar, srv ArticleServer) {
	s.RegisterService(&Article_ServiceDesc, srv)
}

func _Article_FindAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ArticleServer).FindAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.Article/FindAll",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ArticleServer).FindAll(ctx, req.(*Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Article_FindById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FilterById)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ArticleServer).FindById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.Article/FindById",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ArticleServer).FindById(ctx, req.(*FilterById))
	}
	return interceptor(ctx, in, info, handler)
}

func _Article_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NewArticle)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ArticleServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.Article/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ArticleServer).Create(ctx, req.(*NewArticle))
	}
	return interceptor(ctx, in, info, handler)
}

func _Article_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateArticle)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ArticleServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.Article/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ArticleServer).Update(ctx, req.(*UpdateArticle))
	}
	return interceptor(ctx, in, info, handler)
}

// Article_ServiceDesc is the grpc.ServiceDesc for Article service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Article_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "proto.Article",
	HandlerType: (*ArticleServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "FindAll",
			Handler:    _Article_FindAll_Handler,
		},
		{
			MethodName: "FindById",
			Handler:    _Article_FindById_Handler,
		},
		{
			MethodName: "Create",
			Handler:    _Article_Create_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _Article_Update_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "src/transport/grpc/proto/article.proto",
}
