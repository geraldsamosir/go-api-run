// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v4.25.1
// source: src/transport/grpc/proto/article.proto

package go_api_run

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Empty struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *Empty) Reset() {
	*x = Empty{}
	if protoimpl.UnsafeEnabled {
		mi := &file_src_transport_grpc_proto_article_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Empty) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Empty) ProtoMessage() {}

func (x *Empty) ProtoReflect() protoreflect.Message {
	mi := &file_src_transport_grpc_proto_article_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Empty.ProtoReflect.Descriptor instead.
func (*Empty) Descriptor() ([]byte, []int) {
	return file_src_transport_grpc_proto_article_proto_rawDescGZIP(), []int{0}
}

type FilterById struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id int64 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *FilterById) Reset() {
	*x = FilterById{}
	if protoimpl.UnsafeEnabled {
		mi := &file_src_transport_grpc_proto_article_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FilterById) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FilterById) ProtoMessage() {}

func (x *FilterById) ProtoReflect() protoreflect.Message {
	mi := &file_src_transport_grpc_proto_article_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FilterById.ProtoReflect.Descriptor instead.
func (*FilterById) Descriptor() ([]byte, []int) {
	return file_src_transport_grpc_proto_article_proto_rawDescGZIP(), []int{1}
}

func (x *FilterById) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

type UpSertArticleResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Message string `protobuf:"bytes,1,opt,name=message,proto3" json:"message,omitempty"`
	Success bool   `protobuf:"varint,2,opt,name=success,proto3" json:"success,omitempty"`
}

func (x *UpSertArticleResponse) Reset() {
	*x = UpSertArticleResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_src_transport_grpc_proto_article_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpSertArticleResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpSertArticleResponse) ProtoMessage() {}

func (x *UpSertArticleResponse) ProtoReflect() protoreflect.Message {
	mi := &file_src_transport_grpc_proto_article_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpSertArticleResponse.ProtoReflect.Descriptor instead.
func (*UpSertArticleResponse) Descriptor() ([]byte, []int) {
	return file_src_transport_grpc_proto_article_proto_rawDescGZIP(), []int{2}
}

func (x *UpSertArticleResponse) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

func (x *UpSertArticleResponse) GetSuccess() bool {
	if x != nil {
		return x.Success
	}
	return false
}

type NewArticle struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Title  string  `protobuf:"bytes,2,opt,name=title,proto3" json:"title,omitempty"`
	Body   string  `protobuf:"bytes,3,opt,name=body,proto3" json:"body,omitempty"`
	TagsId []int64 `protobuf:"varint,4,rep,packed,name=tags_id,json=tagsId,proto3" json:"tags_id,omitempty"`
}

func (x *NewArticle) Reset() {
	*x = NewArticle{}
	if protoimpl.UnsafeEnabled {
		mi := &file_src_transport_grpc_proto_article_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *NewArticle) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*NewArticle) ProtoMessage() {}

func (x *NewArticle) ProtoReflect() protoreflect.Message {
	mi := &file_src_transport_grpc_proto_article_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use NewArticle.ProtoReflect.Descriptor instead.
func (*NewArticle) Descriptor() ([]byte, []int) {
	return file_src_transport_grpc_proto_article_proto_rawDescGZIP(), []int{3}
}

func (x *NewArticle) GetTitle() string {
	if x != nil {
		return x.Title
	}
	return ""
}

func (x *NewArticle) GetBody() string {
	if x != nil {
		return x.Body
	}
	return ""
}

func (x *NewArticle) GetTagsId() []int64 {
	if x != nil {
		return x.TagsId
	}
	return nil
}

type UpdateArticle struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id      int64       `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Article *NewArticle `protobuf:"bytes,2,opt,name=article,proto3" json:"article,omitempty"`
}

func (x *UpdateArticle) Reset() {
	*x = UpdateArticle{}
	if protoimpl.UnsafeEnabled {
		mi := &file_src_transport_grpc_proto_article_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateArticle) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateArticle) ProtoMessage() {}

func (x *UpdateArticle) ProtoReflect() protoreflect.Message {
	mi := &file_src_transport_grpc_proto_article_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateArticle.ProtoReflect.Descriptor instead.
func (*UpdateArticle) Descriptor() ([]byte, []int) {
	return file_src_transport_grpc_proto_article_proto_rawDescGZIP(), []int{4}
}

func (x *UpdateArticle) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *UpdateArticle) GetArticle() *NewArticle {
	if x != nil {
		return x.Article
	}
	return nil
}

type ListArticles struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Success  bool        `protobuf:"varint,1,opt,name=Success,proto3" json:"Success,omitempty"`
	Articles []*Articles `protobuf:"bytes,2,rep,name=articles,proto3" json:"articles,omitempty"`
}

func (x *ListArticles) Reset() {
	*x = ListArticles{}
	if protoimpl.UnsafeEnabled {
		mi := &file_src_transport_grpc_proto_article_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListArticles) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListArticles) ProtoMessage() {}

func (x *ListArticles) ProtoReflect() protoreflect.Message {
	mi := &file_src_transport_grpc_proto_article_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListArticles.ProtoReflect.Descriptor instead.
func (*ListArticles) Descriptor() ([]byte, []int) {
	return file_src_transport_grpc_proto_article_proto_rawDescGZIP(), []int{5}
}

func (x *ListArticles) GetSuccess() bool {
	if x != nil {
		return x.Success
	}
	return false
}

func (x *ListArticles) GetArticles() []*Articles {
	if x != nil {
		return x.Articles
	}
	return nil
}

type Articles struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id     int64   `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Title  string  `protobuf:"bytes,2,opt,name=title,proto3" json:"title,omitempty"`
	Body   string  `protobuf:"bytes,3,opt,name=body,proto3" json:"body,omitempty"`
	TagsId []int64 `protobuf:"varint,4,rep,packed,name=tags_id,json=tagsId,proto3" json:"tags_id,omitempty"`
}

func (x *Articles) Reset() {
	*x = Articles{}
	if protoimpl.UnsafeEnabled {
		mi := &file_src_transport_grpc_proto_article_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Articles) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Articles) ProtoMessage() {}

func (x *Articles) ProtoReflect() protoreflect.Message {
	mi := &file_src_transport_grpc_proto_article_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Articles.ProtoReflect.Descriptor instead.
func (*Articles) Descriptor() ([]byte, []int) {
	return file_src_transport_grpc_proto_article_proto_rawDescGZIP(), []int{6}
}

func (x *Articles) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *Articles) GetTitle() string {
	if x != nil {
		return x.Title
	}
	return ""
}

func (x *Articles) GetBody() string {
	if x != nil {
		return x.Body
	}
	return ""
}

func (x *Articles) GetTagsId() []int64 {
	if x != nil {
		return x.TagsId
	}
	return nil
}

var File_src_transport_grpc_proto_article_proto protoreflect.FileDescriptor

var file_src_transport_grpc_proto_article_proto_rawDesc = []byte{
	0x0a, 0x26, 0x73, 0x72, 0x63, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2f,
	0x67, 0x72, 0x70, 0x63, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x61, 0x72, 0x74, 0x69, 0x63,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x05, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22,
	0x07, 0x0a, 0x05, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x1c, 0x0a, 0x0a, 0x46, 0x69, 0x6c, 0x74,
	0x65, 0x72, 0x42, 0x79, 0x49, 0x64, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x22, 0x4b, 0x0a, 0x15, 0x55, 0x70, 0x53, 0x65, 0x72, 0x74,
	0x41, 0x72, 0x74, 0x69, 0x63, 0x6c, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x73, 0x75, 0x63,
	0x63, 0x65, 0x73, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x08, 0x52, 0x07, 0x73, 0x75, 0x63, 0x63,
	0x65, 0x73, 0x73, 0x22, 0x4f, 0x0a, 0x0a, 0x6e, 0x65, 0x77, 0x41, 0x72, 0x74, 0x69, 0x63, 0x6c,
	0x65, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x05, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x62, 0x6f, 0x64, 0x79, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x62, 0x6f, 0x64, 0x79, 0x12, 0x17, 0x0a, 0x07, 0x74,
	0x61, 0x67, 0x73, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x03, 0x28, 0x03, 0x52, 0x06, 0x74, 0x61,
	0x67, 0x73, 0x49, 0x64, 0x22, 0x4c, 0x0a, 0x0d, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x41, 0x72,
	0x74, 0x69, 0x63, 0x6c, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x2b, 0x0a, 0x07, 0x61, 0x72, 0x74, 0x69, 0x63, 0x6c, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x11, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x6e,
	0x65, 0x77, 0x41, 0x72, 0x74, 0x69, 0x63, 0x6c, 0x65, 0x52, 0x07, 0x61, 0x72, 0x74, 0x69, 0x63,
	0x6c, 0x65, 0x22, 0x55, 0x0a, 0x0c, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x72, 0x74, 0x69, 0x63, 0x6c,
	0x65, 0x73, 0x12, 0x18, 0x0a, 0x07, 0x53, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x08, 0x52, 0x07, 0x53, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x12, 0x2b, 0x0a, 0x08,
	0x61, 0x72, 0x74, 0x69, 0x63, 0x6c, 0x65, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x0f,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x41, 0x72, 0x74, 0x69, 0x63, 0x6c, 0x65, 0x73, 0x52,
	0x08, 0x61, 0x72, 0x74, 0x69, 0x63, 0x6c, 0x65, 0x73, 0x22, 0x5d, 0x0a, 0x08, 0x41, 0x72, 0x74,
	0x69, 0x63, 0x6c, 0x65, 0x73, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x62,
	0x6f, 0x64, 0x79, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x62, 0x6f, 0x64, 0x79, 0x12,
	0x17, 0x0a, 0x07, 0x74, 0x61, 0x67, 0x73, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x03, 0x28, 0x03,
	0x52, 0x06, 0x74, 0x61, 0x67, 0x73, 0x49, 0x64, 0x32, 0xe0, 0x01, 0x0a, 0x07, 0x41, 0x72, 0x74,
	0x69, 0x63, 0x6c, 0x65, 0x12, 0x2c, 0x0a, 0x07, 0x46, 0x69, 0x6e, 0x64, 0x41, 0x6c, 0x6c, 0x12,
	0x0c, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a, 0x13, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x72, 0x74, 0x69, 0x63, 0x6c,
	0x65, 0x73, 0x12, 0x2e, 0x0a, 0x08, 0x46, 0x69, 0x6e, 0x64, 0x42, 0x79, 0x49, 0x64, 0x12, 0x11,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x42, 0x79, 0x49,
	0x64, 0x1a, 0x0f, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x41, 0x72, 0x74, 0x69, 0x63, 0x6c,
	0x65, 0x73, 0x12, 0x39, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x11, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x6e, 0x65, 0x77, 0x41, 0x72, 0x74, 0x69, 0x63, 0x6c, 0x65, 0x1a,
	0x1c, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x55, 0x70, 0x53, 0x65, 0x72, 0x74, 0x41, 0x72,
	0x74, 0x69, 0x63, 0x6c, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3c, 0x0a,
	0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x14, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x41, 0x72, 0x74, 0x69, 0x63, 0x6c, 0x65, 0x1a, 0x1c, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x55, 0x70, 0x53, 0x65, 0x72, 0x74, 0x41, 0x72, 0x74, 0x69,
	0x63, 0x6c, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x25, 0x5a, 0x23, 0x67,
	0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x67, 0x65, 0x72, 0x61, 0x6c, 0x64,
	0x73, 0x61, 0x6d, 0x6f, 0x73, 0x69, 0x72, 0x2f, 0x67, 0x6f, 0x2d, 0x61, 0x70, 0x69, 0x2d, 0x72,
	0x75, 0x6e, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_src_transport_grpc_proto_article_proto_rawDescOnce sync.Once
	file_src_transport_grpc_proto_article_proto_rawDescData = file_src_transport_grpc_proto_article_proto_rawDesc
)

func file_src_transport_grpc_proto_article_proto_rawDescGZIP() []byte {
	file_src_transport_grpc_proto_article_proto_rawDescOnce.Do(func() {
		file_src_transport_grpc_proto_article_proto_rawDescData = protoimpl.X.CompressGZIP(file_src_transport_grpc_proto_article_proto_rawDescData)
	})
	return file_src_transport_grpc_proto_article_proto_rawDescData
}

var file_src_transport_grpc_proto_article_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_src_transport_grpc_proto_article_proto_goTypes = []interface{}{
	(*Empty)(nil),                 // 0: proto.Empty
	(*FilterById)(nil),            // 1: proto.FilterById
	(*UpSertArticleResponse)(nil), // 2: proto.UpSertArticleResponse
	(*NewArticle)(nil),            // 3: proto.newArticle
	(*UpdateArticle)(nil),         // 4: proto.UpdateArticle
	(*ListArticles)(nil),          // 5: proto.ListArticles
	(*Articles)(nil),              // 6: proto.Articles
}
var file_src_transport_grpc_proto_article_proto_depIdxs = []int32{
	3, // 0: proto.UpdateArticle.article:type_name -> proto.newArticle
	6, // 1: proto.ListArticles.articles:type_name -> proto.Articles
	0, // 2: proto.Article.FindAll:input_type -> proto.Empty
	1, // 3: proto.Article.FindById:input_type -> proto.FilterById
	3, // 4: proto.Article.Create:input_type -> proto.newArticle
	4, // 5: proto.Article.Update:input_type -> proto.UpdateArticle
	5, // 6: proto.Article.FindAll:output_type -> proto.ListArticles
	6, // 7: proto.Article.FindById:output_type -> proto.Articles
	2, // 8: proto.Article.Create:output_type -> proto.UpSertArticleResponse
	2, // 9: proto.Article.Update:output_type -> proto.UpSertArticleResponse
	6, // [6:10] is the sub-list for method output_type
	2, // [2:6] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_src_transport_grpc_proto_article_proto_init() }
func file_src_transport_grpc_proto_article_proto_init() {
	if File_src_transport_grpc_proto_article_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_src_transport_grpc_proto_article_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Empty); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_src_transport_grpc_proto_article_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FilterById); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_src_transport_grpc_proto_article_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpSertArticleResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_src_transport_grpc_proto_article_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*NewArticle); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_src_transport_grpc_proto_article_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateArticle); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_src_transport_grpc_proto_article_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListArticles); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_src_transport_grpc_proto_article_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Articles); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_src_transport_grpc_proto_article_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_src_transport_grpc_proto_article_proto_goTypes,
		DependencyIndexes: file_src_transport_grpc_proto_article_proto_depIdxs,
		MessageInfos:      file_src_transport_grpc_proto_article_proto_msgTypes,
	}.Build()
	File_src_transport_grpc_proto_article_proto = out.File
	file_src_transport_grpc_proto_article_proto_rawDesc = nil
	file_src_transport_grpc_proto_article_proto_goTypes = nil
	file_src_transport_grpc_proto_article_proto_depIdxs = nil
}
