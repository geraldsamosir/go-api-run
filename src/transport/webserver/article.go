package webserver

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/geraldsamosir/go-api-run/src/entity"
	"gitlab.com/geraldsamosir/go-api-run/src/helper"
	"gitlab.com/geraldsamosir/go-api-run/src/service/article"
)

type ArticleHandler struct {
	articleSvc article.Article
}

func NewArticleHandler(e *echo.Group, articleSvc article.Article) {
	handler := &ArticleHandler{
		articleSvc: articleSvc,
	}

	e.GET("/articles", handler.FindAll)
	e.GET("/articles/:articleId", handler.FindById)
	e.POST("/articles", handler.Create)
	e.PATCH("/articles/:articleId", handler.Update)
}

func (ah *ArticleHandler) FindAll(c echo.Context) error {
	ctx := c.Request().Context()
	articles, err := ah.articleSvc.FindAll(ctx)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{
			"message": "server error",
			"error":   err.Error(),
		})
	}

	return c.JSON(http.StatusOK, map[string]any{
		"message": "Berhasil mendapatkan article",
		"data":    articles,
	})
}

func (ah *ArticleHandler) FindById(c echo.Context) error {
	ctx := c.Request().Context()
	articleId, _ := strconv.Atoi(c.Param("articleId"))

	article, err := ah.articleSvc.FindById(ctx, articleId)

	if err != nil {
		return c.JSON(http.StatusNotFound, map[string]string{
			"message": "Article tidak ditemukan",
		})
	}

	return c.JSON(http.StatusOK, map[string]any{
		"message": "Article berhasil ditemukan",
		"data":    article,
	})
}

func (ah *ArticleHandler) Create(c echo.Context) error {
	ctx := c.Request().Context()

	articlePayload := entity.ArticleEntity{}
	err := c.Bind(&articlePayload)

	if err != nil {
		fmt.Println("article.create failed bind", err.Error())
		return c.JSON(http.StatusInternalServerError, map[string]string{
			"message": "Gagal Membuat artikel",
		})
	}

	validation := helper.ValidationRequest{}

	if err := validation.ValidateHandling(&articlePayload); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]any{
			"message": "payload invalid",
			"error":   err,
		})
	}

	isSuccessCreate, err := ah.articleSvc.Create(ctx, articlePayload)

	if !isSuccessCreate || err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{
			"message": "Gagal Membuat artikel",
		})
	}

	return c.JSON(http.StatusOK, map[string]string{
		"message": "Berhasil membuat Artikel",
	})

}

func (ah *ArticleHandler) Update(c echo.Context) error {
	ctx := c.Request().Context()

	articleId, _ := strconv.Atoi(c.Param("articleId"))

	articlePayload := entity.ArticleEntity{}
	err := c.Bind(&articlePayload)

	if err != nil {
		fmt.Println("article.update failed bind", err.Error())
		return c.JSON(http.StatusInternalServerError, map[string]string{
			"message": "Gagal Memperbarui artikel",
		})
	}

	isSuccessCreate, err := ah.articleSvc.Update(ctx, articleId, articlePayload)

	if !isSuccessCreate || err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{
			"message": "Gagal Memperbarui artikel",
		})
	}

	return c.JSON(http.StatusOK, map[string]string{
		"message": "Berhasil Memperbarui Artikel",
	})

}
