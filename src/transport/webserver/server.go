package webserver

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"gitlab.com/geraldsamosir/go-api-run/src/service/article"
	"gitlab.com/geraldsamosir/go-api-run/src/service/movie"
)

type WebServer struct{}

func (web *WebServer) Run(articleSvc article.Article, movieSvc movie.Movie) {
	e := echo.New()

	//e.Use(middleware.Logger())

	// allow cors to access api
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))

	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
	}))

	e.Use(middleware.SecureWithConfig(middleware.DefaultSecureConfig))

	e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]any{
			"message": "hello this is api run",
		})
	})

	api := e.Group("/api")

	NewArticleHandler(api, articleSvc)
	NewMovieHandler(api, movieSvc)

	log.Info(e.Start(":9092"))

}
