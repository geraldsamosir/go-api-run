package webserver

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/geraldsamosir/go-api-run/src/service/movie"
)

type MovieHandler struct {
	movieSvc movie.Movie
}

func NewMovieHandler(e *echo.Group, movieSvc movie.Movie) {
	handler := &MovieHandler{
		movieSvc: movieSvc,
	}

	e.GET("/movies", handler.FindAll)
}

func (mh *MovieHandler) FindAll(c echo.Context) error {
	ctx := c.Request().Context()

	movies, err := mh.movieSvc.FindAll(ctx, c.QueryParam("title"), c.QueryParam("page"))

	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{
			"message": "server error",
			"error":   err.Error(),
		})
	}

	return c.JSON(http.StatusOK, map[string]any{
		"message": "Berhasil mendapatkan article",
		"data":    movies,
	})
}
