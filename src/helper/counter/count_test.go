package helper

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCounter(t *testing.T) {
	answer := Count(1, 1)
	fmt.Println("result", answer)
	assert.Equal(t, answer, 2)
}
